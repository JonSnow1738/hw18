#include <iostream>
#include <string> 
#include <Windows.h>
#include "Helper.h"
#include <vector>
#include <fstream>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>

typedef short (CALLBACK* FindArtistType)(LPCTSTR);
#define MAX_PATH 2048
using namespace ::std;
string getCurrPath();
void cd(string newPath);
void createFile(string file);
int getdir(string dir, vector<string> &files);
int secret();
int toOctalFromDecimal(int num);