#include "source.h"

int main()
{
	string line;
	vector<string> files,commend;
	int retValue;

	while (1){

		getline(cin, line);
		commend = Helper::get_words(line);
		
		/*
			check the coomend and call function in accordance
		*/
		if (commend.at(0) == "pwd"){
			cout << "The current directory is " << getCurrPath() << endl;//getCurrPath()  return the curr dir
		}
		else if (commend.at(0) == "cd"){
			cd(commend.at(1));//send the name of folder to cd in
			cout << "The current directory is " << getCurrPath() << endl;
		}
		else if (commend.at(0) == "create"){
			createFile(commend.at(1));
		}
		else if (commend.at(0) == "ls"){//getdir function push to the files vector the files that in the curr folder then print the files 
			getdir(getCurrPath(), files);
			for (vector<string>::const_iterator i = files.begin(); i != files.end(); ++i)
			{
				std::cout << *i << endl;

			}

		}
		else if (commend.at(0) == "secret"){
			retValue = secret();
			cout << "The function returned " << to_string(retValue) << std::endl;
		}
		else if (commend.at(0).substr(commend.at(0).length() - 4) == ".exe"){
			string path = getCurrPath();//build the full path
			path += commend.at(0);
			ShellExecute(NULL, "open", path.c_str(), NULL, NULL, SW_SHOWDEFAULT);
		}
		else{
			cout << "Wrong!!" << endl;
		}

		


	}
}

string getCurrPath() {
	DWORD currentDir = MAX_PATH;
	char sizeCurDir[MAX_PATH];
	GetCurrentDirectory(currentDir, sizeCurDir);
	string ret(sizeCurDir);
	return ret;
}

void cd(string newPath){
	BOOL dir = SetCurrentDirectory(newPath.c_str());
}

void createFile(string file){
	ofstream myfile;
	myfile.open(file);
	myfile.clear();
}

/*
get the path and a vector open the dir and pushing the names to the string vector
*/
int getdir(string dir, vector<string> &files)
{
	DIR *dp;
	struct dirent *file;
	if ((dp = opendir(dir.c_str())) == NULL) {
		cout << "Error(" << errno << ") opening " << dir << endl;
		return errno;
	}

	while ((file = readdir(dp)) != NULL) {
		files.push_back(string(file->d_name));
	}
	closedir(dp);
	return 0;
}


int secret(){
	HINSTANCE hGetProcIDDLL = LoadLibrary("secret.dll");

	if (!hGetProcIDDLL) {
		std::cout << "could not load the dynamic library" << std::endl;
		return EXIT_FAILURE;
	}

	int ans = (char)GetProcAddress(hGetProcIDDLL, "TheAnswerToLifeTheUniverseAndEverything");
	ans = toOctalFromDecimal(ans);
	if (!ans) {
		std::cout << "could not locate the function" << std::endl;
		return EXIT_FAILURE;
	}

	return ans;
}

int toOctalFromDecimal(int num) {
	ostringstream o;
	o << std::oct << num;
	num = stoi(o.str());
	return num;
}

